<?php

/**
 * Implementation of hook_field_info().
 * D7
 */
function streamingmediafield_field_info() {
  return array(
      'streaming_media' => array(
          'label' => t('Streaming Media'),
          'description' => t('Live or on-demand streaming media'),
      ),
  );
}

/**
 * Implementation of hook_field_schema().
 * D7
 */
function streamingmediafield_field_schema() {
  $columns = array();
  $columns['stream_name'] = array('type' => 'varchar', 'length' => 255, 'not null' => TRUE, 'default' => '');
  $columns['stream_path'] = array('type' => 'varchar', 'length' => 255, 'not null' => FALSE);
  $columns['is_live'] = array('type' => 'int', 'size' => 'tiny', 'not null' => TRUE, 'default' => 0);
  $columns['progressive_download'] = array('type' => 'int', 'size' => 'tiny', 'not null' => TRUE, 'default' => 0);

  $indexes = array();
  $indexes['stream_name'] = array('stream_name');

  return array(
      'columns' => $columns,
      'indexes' => $indexes,
  );
}

/**
 * Implementation of hook_field_schema().
 * D7
 */

/**
 * Implementation of hook_field_is_empty().
 * D7
 */
function streamingmediafield_field_is_empty($item, $field) {
  return empty($item['stream_name']);
}

/**
 * Implementation of hook_field_formatter_info().
 * D7
 */
function streamingmediafield_field_formatter_info() {
  return array(
      'default' => array(
          'label' => t('Default'),
          'field types' => array('streaming_media'),
      ),
  );
}

/**
 * Implementation of hook_field_formatter_view().
 * D7
 */
function streamingmediafield_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();

  switch ($display['type']) {
    case 'default':
      foreach ($items as $delta => $item) {
        $element[$delta]['#markup'] = t('Stream output should go here.  Stream name is !stream', array('!stream' => $item['stream_name']));
      }
      break;
  }

  return $element;
}

/**
 * Implementation of hook_field_widget_info().
 * D7
 */
function streamingmediafield_field_widget_info() {
  $info = array();

  $info['streaming_media'] = array(
      'label' => t('Streaming media config'),
      'field types' => array('streaming_media'),
  );

  return $info;
}

/**
 * Implementation of hook_field_widget_form()
 * D7 ... in progress
 */
function streamingmediafield_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  $stream_type_chooser = $instance['widget']['settings']['stream_type'];

  if (isset($items[$delta])) {
    $progressive_download_default = ($form['nid']['#value']) ? $items[$delta]['progressive_download'] : FALSE;
    $is_live = $items[$delta]['is_live'];
    $stream_path = $items[$delta]['stream_path'];
    $stream_name = $items[$delta]['stream_name'];
  }
  else {
    $progressive_download_default = (isset($instance['widget']['settings']['progressive_download_default'])) ? $instance['widget']['settings']['progressive_download_default'] : FALSE;
    $is_live = 0;
    $stream_path = '';
    $stream_name = '';
  }

  $element += array(
      '#delta' => $delta,
      '#type' => 'fieldset',
  );
  $base = $element;


  if ($stream_type_chooser == 'choose') {
    $element['is_live'] = array(
        '#type' => 'radios',
        '#title' => t('Stream Type'),
        '#options' => array(
            1 => t('Live Stream'),
            0 => t('On Demand Stream'),
        ),
        '#description' => t('A live stream plays from a live video feed on the server. An on demand stream is stored on the server and can be played any time.'),
        '#default_value' => $is_live,
        '#weight' => 0,
            ) + $base;
  }
  else {
    $element['is_live'] = array('#type' => 'hidden', '#value' => ($stream_type_chooser == 'live') ? 1 : 0) + $base;
  }

  if ($stream_type_chooser == 'choose' || $stream_type_chooser == 'ondemand') {
    $element['stream_path'] = array(
        '#type' => 'textfield',
        '#title' => t('Stream Path'),
        '#description' => t('The directory on the streaming server where this stream can be found'),
        '#default_value' => $stream_path,
        '#weight' => 1,
            ) + $base;
    $element['progressive_download'] = array(
        '#type' => 'checkbox',
        '#title' => t('Use progressive download instead of streaming'),
        '#description' => t('If checked, the player will use progressive download technology rather than streaming.'),
        '#default_value' => (bool) $progressive_download_default,
        '#weight' => 5,
            ) + $base;

    if (module_exists('ctools')) {
      $field_key = sprintf('%s[%s][%d][is_live]', $field['field_name'], $langcode, $delta);
      ctools_include('dependent');
      $element['stream_path']['#dependency'] = array('radio:' . $field_key => array(0));
      $element['progressive_download']['#dependency'] = array('radio:' . $field_key => array(0));
    }
  }
  else {
    $element['progressive_download'] = array('#type' => 'value', '#value' => 0) + $base;
  }

  $element['stream_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Stream Name'),
      '#description' => t('The name of the media stream'),
      '#default_value' => $stream_name,
      '#weight' => 3,
          ) + $base;

  return $element;
}

/**
 * Implements hook_field_widget_settings_form().
 * D7
 */
function streamingmediafield_field_widget_settings_form($field, $instance) {
  $widget = $instance['widget'];
  $defaults = field_info_widget_settings($widget['type']);
  $settings = array_merge($defaults, $widget['settings']);

  $form = array();
  $form['stream_type'] = array(
      '#type' => 'select',
      '#title' => t('Stream Type'),
      '#options' => array(
          'live' => t('Live Stream'),
          'ondemand' => t('On Demand Stream'),
          'choose' => t('Allow User to Choose Per Node'),
      ),
      '#default_value' => ($settings['stream_type']) ? $settings['stream_type'] : 'choose',
  );
  $form['collapsible'] = array(
      '#type' => 'checkbox',
      '#title' => t('Allow user to collapse this field'),
      '#default_value' => isset($settings['collapsible']) ? $settings['collapsible'] : TRUE,
  );

  $form['progressive_download_default'] = array(
      '#type' => 'checkbox',
      '#title' => t('Select progressive download by default for on-demand content'),
      '#default_value' => isset($settings['progressive_download_default']) ? $settings['progressive_download_default'] : FALSE,
  );

  return $form;
}

/**
 * Implementation of hook_field_settings_form().
 * D7
 */
function streamingmediafield_field_settings_form($field, $instance, $has_data) {
  $settings = $field['settings'];

  $fields = field_info_instances($instance['entity_type'], $instance['bundle']);

  $form = array();

  foreach ($fields as $field_name => $f) {

    $field_info = field_info_field($field_name);

    if ($field_info['type'] == 'image') {
      $imagefield_options[$field_name] = $f['label'];
    }
  }

  if (!empty($imagefield_options)) {
    $form['still_image_field'] = array(
        '#type' => 'select',
        '#title' => t('Still Image Field'),
        '#options' => $imagefield_options,
        '#default_value' => $settings['still_image_field'],
    );
  }
  return $form;
}

/* * ***************************************************** */

/**
 * Implementation of hook_menu().
 */
function streamingmediafield_menu() {
  $items = array();

  $items['admin/config/streamingmediafield'] = array(
      'title' => 'Streaming Media Field',
      'position' => 'right',
      'weight' => -5,
      'page callback' => 'system_admin_menu_block_page',
      'access arguments' => array('administer site configuration'),
      'file' => 'system.admin.inc',
      'file path' => drupal_get_path('module', 'system'),
  );

  $items['admin/config/streamingmediafield/settings'] = array(
      'title' => 'Global Settings',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('streamingmediafield_admin_settings_form'),
      'access arguments' => array('administer site configuration'),
      'file' => 'includes/admin.inc',
  );

  return $items;
}
